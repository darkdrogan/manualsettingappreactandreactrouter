import { MainMenu } from './MainMenu';

const LikePageTemplate = ({children}) =>
  <div className='page'>
    <MainMenu/>
    {children}
  </div>

export default LikePageTemplate