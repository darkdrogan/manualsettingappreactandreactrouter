import React from 'react'
import { NavLink } from 'react-router-dom'

export const MainMenu = () =>
  <div className='home'>
    <nav>
      <NavLink to='about'>[About]</NavLink>
      <NavLink to='events'>[Events]</NavLink>
      <NavLink to='products'>[Products]</NavLink>
      <NavLink to='contact'>[Contact us]</NavLink>
    </nav>
  </div>