import React from 'react'
import {MainMenu} from './components/MainMenu';
import LikePageTemplate from './components/LikePageTemplate'

export const Home = () =>
  <LikePageTemplate>
    <MainMenu/>
  </LikePageTemplate>

export const About = () =>
  <LikePageTemplate>
    <section className='about'>
      <h1>[About the Company]</h1>
    </section>
  </LikePageTemplate>

export const Events = () =>
  <LikePageTemplate>
    <section className='events'>
      <h1>[Events Calendar]</h1>
    </section>
  </LikePageTemplate>

export const Products = () =>
  <LikePageTemplate>
    <section className='products'>
      <h1>[Products Catalog]</h1>
    </section>
  </LikePageTemplate>

export const Contact = () =>
  <LikePageTemplate>
    <section className='contact'>
      <h1>[Contact Us]</h1>
    </section>
  </LikePageTemplate>

export const Whoops404 = ({ location }) =>
    <div className='whoops-404'>
      <h1>Resource not fount at {location.pathname}</h1>
    </div>
