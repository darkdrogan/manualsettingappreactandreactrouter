const path = require('path');

module.exports = {
  entry: './src',
  output: {
    path: __dirname,
    filename: 'build.js'
  },
  module: {
    rules: [
      {
        test: /.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      }
    ]
  },
  devServer: {
    compress: true,
    contentBase: __dirname + '/public',
    port: 4000
  }
}